package br.com.cvc.template.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cvc.template.exception.ResourceNotFoundException;
import br.com.cvc.template.model.Pessoa;
import br.com.cvc.template.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping(path = "/api/v1/cvccorp")
public class PessoaController {
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@GetMapping("/pessoas")
	public List<Pessoa> getAllPessoas() {
		return pessoaRepository.findAll();
	}

	@GetMapping("/pessoas/{id}")
	public ResponseEntity<Pessoa> getPessoaById(@PathVariable(value = "id") Long pessoaId)
			throws ResourceNotFoundException {
		Pessoa pessoa = pessoaRepository.findById(pessoaId)
				.orElseThrow(() -> new ResourceNotFoundException("Pessoa not found for this id :: " + pessoaId));
		return ResponseEntity.ok().body(pessoa);
	}

	@PostMapping("/pessoas")
	public Pessoa createPessoa(@Valid @RequestBody Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}

	@PutMapping("/pessoas/{id}")
	public ResponseEntity<Pessoa> updatePessoa(@PathVariable(value = "id") Long pessoaId,
			@Valid @RequestBody Pessoa pessoaDetails) throws ResourceNotFoundException {
		
		log.info("Inicia busca da Pessoa pelo id");
		Pessoa pessoa = pessoaRepository.findById(pessoaId)
				.orElseThrow(() -> new ResourceNotFoundException("Pessoa not found for this id :: " + pessoaId));

		pessoa.setIdPessoa(pessoaDetails.getIdPessoa());
		pessoa.setNome(pessoaDetails.getNome());
		pessoa.setSobrenome(pessoaDetails.getSobrenome());
		log.info("Altera a Pessoa");
		final Pessoa updatedPessoa = pessoaRepository.saveAndFlush(pessoa);
		return ResponseEntity.ok(updatedPessoa);
	}

	@DeleteMapping("/pessoas/{id}")
	public Map<String, Boolean> deletePessoa(@PathVariable(value = "id") Long pessoaId)
			throws ResourceNotFoundException {
		
		log.info("Inicia busca da Pessoa pelo id");
		Pessoa pessoa = pessoaRepository.findById(pessoaId)
				.orElseThrow(() -> new ResourceNotFoundException("Pessoa not found for this id :: " + pessoaId));

		log.info("Apaga a Pessoa");
		pessoaRepository.delete(pessoa);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
