package br.com.cvc.template.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cvc.template.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

}
