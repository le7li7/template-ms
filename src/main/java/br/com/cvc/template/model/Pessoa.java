package br.com.cvc.template.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Entity
@Table(name = "pessoa")
@NamedQueries({@NamedQuery(name = "Pessoa.findAll", query = "SELECT l FROM Pessoa l")})
public class Pessoa {
	
    @Id
    @Column(name = "id_pessoa", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPessoa;
    
    @Basic
    @Column(name = "nome", length = 100, nullable = true)
    private String nome;
    
    @Basic
    @Column(name = "sobrenome", length = 100)
    private String sobrenome;
 
}
