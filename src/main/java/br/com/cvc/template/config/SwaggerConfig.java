package br.com.cvc.template.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

	private static final String DESCRIPTION_TITLE = "API Template CVC official documentation.\n\n";
	private static final String DESCRIPTION_RELEASE_UPDATES = "Last updates: \n"
		    												+ "- Building property and converter classes;\n"
		    												+ "- Updating request/response model classes;\n"
		    												+ "- Applying @RestControllerAdvice in handler classes;\n"
		    												+ "- Updating template-ms to version 0.0.1-SNAPSHOT;\n";
	
	@Bean
	public Docket greetingApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.cvc.template.controller")).build().apiInfo(metaData());
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder()
				.title("API Template CVC")
				.description(DESCRIPTION_TITLE + DESCRIPTION_RELEASE_UPDATES)
				.version("1.0.0")
				.contact(new Contact("Leandro Ferreira", null, "leandroferreira@ext.cvccorp.com.br"))
				.build();
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}
